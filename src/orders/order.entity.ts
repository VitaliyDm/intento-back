import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { OrderStatus } from './interfaces/IOrder';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: OrderStatus.inProgress })
  status: OrderStatus;
}
