import { OrderController } from '../order.controller';
import { OrderService } from '../order.service';
import { Repository } from 'typeorm';
import { TestScheduler } from 'rxjs/testing';

describe('OrderController', () => {
  let orderController;
  let orderService;
  let repository;

  beforeEach(() => {
    repository = new Repository();
    orderService = new OrderService(repository);
    orderController = new OrderController(orderService);
  });

  describe('findAll', () => {
    it('should return an array of orders', async () => {
      const result = ['test'];
      jest.spyOn(orderService, 'findAll').mockImplementation(() => result);

      expect(await orderController.findAll()).toBe(result);
    });
  });

  describe('create', () => {
    it('should return order', async () => {
      const result = { id: 1, name: 'test', status: 'inProgress' };
      jest.spyOn(orderService, 'create').mockImplementation(() => result);

      expect(await orderController.create()).toBe(result);
    });
  });
});
