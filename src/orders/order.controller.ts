import { Body, Controller, Get, Post, Sse } from '@nestjs/common';
import { Observable } from 'rxjs';

import { OrderService } from './order.service';
import { CreateOrderDto } from './dto/create-order.dto';
import { Order } from './order.entity';

@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Get()
  findAll(): Promise<Order[]> {
    return this.orderService.findAll();
  }

  @Post()
  async create(@Body() createOrderDto: CreateOrderDto): Promise<Order> {
    return await this.orderService.create(createOrderDto);
  }

  @Sse('/events')
  sse(): Observable<MessageEvent> {
    return this.orderService.getUpdateEvents();
  }
}
