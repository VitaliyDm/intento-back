enum OrderStatus {
  inProgress = 'inProgress',
  done = 'done',
  failed = 'failed',
}

interface IOrder {
  name: string;
  status: OrderStatus;
}

export { IOrder, OrderStatus };
