import { Order } from '../order.entity';

interface IObserverMessageData {
  data: {
    order: Order;
  };
}

export { IObserverMessageData };
