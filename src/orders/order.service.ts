import { Injectable } from '@nestjs/common';
import { Observable, Subject } from 'rxjs';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Order } from './order.entity';

import { CreateOrderDto } from './dto/create-order.dto';

import { OrderStatus } from './interfaces/IOrder';
import { IObserverMessageData } from './interfaces/IObserverMessageData';

const updateStatusTimeout = 5000;

@Injectable()
export class OrderService {
  private readonly events: Subject<MessageEvent<IObserverMessageData>>;
  constructor(
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
  ) {
    this.events = new Subject();
  }

  findAll(): Promise<Order[]> {
    return this.orderRepository.find();
  }

  create(createOrderDto: CreateOrderDto): Promise<Order> {
    const order = new Order();
    order.name = createOrderDto.name;

    setTimeout(async () => {
      const newOrder = await this.update(
        order,
        Math.random() < 0.5 ? OrderStatus.failed : OrderStatus.done,
      );

      this.events.next({ data: { order: newOrder } } as MessageEvent);
    }, updateStatusTimeout);

    return this.orderRepository.save(order);
  }

  update(order: Order, status: OrderStatus): Promise<Order> {
    return this.orderRepository.save({
      ...order,
      status,
    });
  }

  getUpdateEvents(): Observable<MessageEvent> {
    return this.events;
  }
}
