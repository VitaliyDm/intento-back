export const PORT = process.env.PORT || 3000;
export const DB_TYPE = process.env.DB_TYPE;
export const DB_HOST = process.env.DB_HOST || 'postgres';
export const DB_PORT = parseInt(process.env.DB_PORT);
export const DB_USER = process.env.DB_USER;
export const DB_PASS = process.env.DB_PASS;
export const DB_NAMESPACE = process.env.DB_NAMESPACE;
