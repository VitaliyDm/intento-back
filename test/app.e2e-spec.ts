import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const orderService = {
    findAll: () => [{ id: 1, name: 'test', status: 'failed' }],
    create: () => ({ id: 1, name: 'test', status: 'inProgress' }),
    update: () => ({ id: 1, name: 'test', status: 'failed' }),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/order (GET)', () => {
    return request(app.getHttpServer()).get('/order').expect(200).expect({ data: orderService.findAll() });
  });

  afterAll(async () => {
    await app.close();
  });
});
